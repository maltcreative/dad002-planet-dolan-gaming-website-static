console.log('\'Allo \'Allo!');

$(document).ready(function() {
	if ($('.award-contain').hasClass('active')) {
		$('.challenge .about .right .rate-difficulty').removeClass('noclick');
		$('span.difficulty-rate-msg').hide();
	}
});


$(document).ready(function() {
	$(".slides > div:gt(0)").css('top', '-150%');
				     	$('#first .progress').animate({width: '100%'}, 6000).animate({left: '100%'}).animate({width: '0%', left: '0%'}, 0);

	var interval = setInterval(function() {
				     $('.slides > div:first').delay(2000).animate({top: '-150%'}, 0).next().animate({top: '0%'}, 1000).end().appendTo('.slides');
				     	var slidenum = $('.slides > div:first').attr('data-slidenum');
				     	$('#' + slidenum + ' .progress').animate({width: '100%'}, 6000).animate({left: '100%'}).animate({width: '0%', left: '0%'}, 0);
				   },  6000);

				$('.slide-nav .nav-item').on('click', changeSlide);

				function changeSlide(){
					clearInterval(interval);
					$('.slide-nav .nav-item').off('click', changeSlide);
					$('.slides > div').stop().css('z-index', '0');

					var clickedNav = $(this).attr('id');
					$('.slide-nav .nav-item').removeClass('active').addClass('inactive');
					$(this).addClass('active').removeClass('inactive');
				    $('.single-slide[data-slidenum="'+clickedNav+'"]').animate({top: '0%'}, 600, function(){
				     	$('.slide-nav .nav-item').on('click', changeSlide);
					 	$('.single-slide:not([data-slidenum="'+clickedNav+'"])').animate({top: '-150%'}, 0);
				    }).appendTo('.slides');
				}
});


$(document).ready(function() {
	$('.yesno > div').on('click', function(){
		$('.yesno > div').removeClass('active');
		$(this).toggleClass('active');
	});
	$('.challenge .header .icon-award').on('click', function(){
		$(this).toggleClass('active');
	});
	$('.popularity .award-contain').on('click', function(){
		$(this).toggleClass('active');
		$('.challenge .about .right .rate-difficulty').delay(1000).toggleClass('noclick');
		$('span.difficulty-rate-msg').fadeToggle();
	});
});


$(document).ready(function() {
var doubleLabels = [
    "<span class='number'>1<span class='rank'><img src='images/rank01.png'></span></span>",
    "<span class='number'>2<span class='rank'><img src='images/rank01.png'></span></span>",
    "<span class='number'>3<span class='rank'><img src='images/rank02.png'></span></span>",
    "<span class='number'>4<span class='rank'><img src='images/rank02.png'></span></span>",
    "<span class='number'>5<span class='rank'><img src='images/rank02.png'></span></span>",
    "<span class='number'>6<span class='rank'><img src='images/rank03.png'></span></span>",
    "<span class='number'>7<span class='rank'><img src='images/rank03.png'></span></span>",
    "<span class='number'>8<span class='rank'><img src='images/rank03.png'></span></span>",
    "<span class='number'>9<span class='rank'><img src='images/rank04.png'></span></span>",
    "<span class='number'>10<span class='rank'><img src='images/rank04.png'></span></span>"
];

$("#double-label-slider")
    .slider({
        max: 10,
        min: 1,
        value: 5,
        animate: 0
    })
    .slider("float", {
        rest: "label",
        labels: doubleLabels
    })
    .slider("pips");
});

$(document).ready(function() {
var doubleLabels = [
    "<span class='number'>1<span class='rank'><img src='images/rank01.png'></span></span>",
    "<span class='number'>2<span class='rank'><img src='images/rank01.png'></span></span>",
    "<span class='number'>3<span class='rank'><img src='images/rank02.png'></span></span>",
    "<span class='number'>4<span class='rank'><img src='images/rank02.png'></span></span>",
    "<span class='number'>5<span class='rank'><img src='images/rank02.png'></span></span>",
    "<span class='number'>6<span class='rank'><img src='images/rank03.png'></span></span>",
    "<span class='number'>7<span class='rank'><img src='images/rank03.png'></span></span>",
    "<span class='number'>8<span class='rank'><img src='images/rank03.png'></span></span>",
    "<span class='number'>9<span class='rank'><img src='images/rank04.png'></span></span>",
    "<span class='number'>10<span class='rank'><img src='images/rank04.png'></span></span>"
];

$("#double-label-slider-sugg")
    .slider({
        max: 10,
        min: 1,
        value: 5,
        animate: 0
    })
    .slider("float", {
        rest: "label",
        labels: doubleLabels
    })
    .slider("pips");
});

$(document).ready(function() {
    $('.tooltip').tooltipster({
    	position: 'right',
		delay: 100,
		animation: 'grow'
    });
});

$(document).ready(function() {
    $('.tooltipdown').tooltipster({
    	position: 'bottom',
		delay: 100,
		animation: 'grow'
    });
});

$(document).ready(function() {
	$('.navigation li.has-children').mouseenter(function() {
		$(this).find('ul.child-menu').fadeIn(200);
		$(this).addClass('active');

		$('.navigation li.has-children ul.child-menu' && this).mouseleave(function() {
			$('.navigation ul.child-menu').fadeOut(100);
			$('.navigation li.has-children').removeClass('active');
		});

	});
});

$(document).ready(function() {
	$('.mobile-nav li.has-children > a').on('click', function(e){
		e.preventDefault();
	})
	$('.mobile-nav li.has-children').on('click', function() {
		$(this).find('ul.child-menu').slideToggle(200);
		$(this).toggleClass('active');
	});
});

$(document).ready(function() {
	var navwidth = $('.navigation .right').outerWidth() + 19;
	$('.toparea .navigation .right .search input').css('max-width', navwidth);
	$('.navigation .search').mouseenter(function() {
		$('.search input').focus();
		$('.navigation .search').mouseleave(function() {
			$('.search input').blur();
		});
	});
});

$(document).ready(function() {
	$('.challenge-feed .challenge-nav .popular').on('click', function() {
		$(this).addClass('active');
		$('.challenge-feed .challenge-nav .latest').removeClass('active');
		$('.feed .popular').addClass('active');
		$('.feed .latest').removeClass('active');
	});
	$('.challenge-feed .challenge-nav .latest').on('click', function() {
		$(this).addClass('active');
		$('.challenge-feed .challenge-nav .popular').removeClass('active');
		$('.feed .latest').addClass('active');
		$('.feed .popular').removeClass('active');
	});
});

$(document).ready(function() {
	$('.hamburger').on('click', function() {
		$(this).toggleClass('active');
		$('.mobile-nav').slideToggle();
	});
	$('.profile-openclose').on('click', function() {
		$(this).toggleClass('active');
		$('.mobile-nav .profile').slideToggle();
	});
});

$(document).ready(function() {
	$('.my-slider').unslider({
		nav: false,
		arrows: {
			//  Unslider default behaviour
			prev: '<a class="unslider-arrow prev"><div class="prev"><div class="icon-carretleft"></div></div></a>',
			next: '<a class="unslider-arrow next"><div class="prev"><div class="icon-carretright"></div></div></a>',
		}
	});
	$('.unslider-arrow.prev').prependTo('.controls');
	$('.unslider-arrow.next').appendTo('.controls');
});

$(document).ready(function() {
	$('.gallery').unslider({
		arrows: false,
		arrows: {
			//  Unslider default behaviour
			prev: '<a class="unslider-arrow prev"><div class="prev"><div class="icon-carretleft"></div></div></a>',
			next: '<a class="unslider-arrow next"><div class="prev"><div class="icon-carretright"></div></div></a>',
		}
	});
	$('.unslider-arrow.prev').prependTo('.controls');
	$('.unslider-arrow.next').appendTo('.controls');
});



$(document).ready(function() {
	$('[data-tab]').on('click', function (e) {
	  $(this).addClass('active').siblings('[data-tab]').removeClass('active')
	  $('.tabs').find('[data-content=' + $(this).data('tab') + ']').addClass('active').siblings('[data-content]').removeClass('active')
	  e.preventDefault()
	});
});

$(document).ready(function() {
	$('.suggestions-list .single-suggestion .header .ti-info .right .view-more').on('click', function() {
		$(this).children().toggleClass('active');
		$(this).parents().toggleClass('active').eq(2).next().slideToggle();
	});
	$('.suggestions-list .single-suggestion .details .right .comments .count').on('click', function() {
		$(this).find('.updowntoggle').toggleClass('active');
		$(this).parent().parent().parent().next('.the-comments').slideToggle();
	});
});

$(document).ready(function() {
	$('.suggestions-list .single-suggestion .header .ti-info .left').on('click', function() {
		$(this).toggleClass('active');
		$(this).parent().find('.updowntoggle').toggleClass('active');
		$(this).parents().toggleClass('active').eq(1).next().slideToggle();
	});
});


$(document).ready(function() {
	$('.suggestions-list .single-suggestion .header .ti-info .right .updown .up').on('click', function() {
		$(this).toggleClass('active');
		$(this).siblings('.down').removeClass('active');
	});
	$('.suggestions-list .single-suggestion .header .ti-info .right .updown .down').on('click', function() {
		$(this).toggleClass('active');
		$(this).siblings('.up').removeClass('active');
	});
});


$(document).ready(function() {
	$('.platform-details.single-game.suggestions .wrap01 .submit').on('click', function() {
		$(this).hide();
		$('.platform-details.single-game.suggestions .wrap01 .cancel').css('display', 'flex');
		$('.platform-details.single-game.suggestions .wrap01 .cancel').on('click', function(){
			$(this).css('display', 'none');
			$('.platform-details.single-game.suggestions .wrap01 .submit').css('display', 'flex');
			$('.platform-details.single-game.suggestions .wrap02').css('display', 'none');
		});
		$('.platform-details.single-game.suggestions .wrap02').css('display', 'flex');
		$('.platform-details.single-game.suggestions .wrap02 .submit').on('click', function(){
			$('.platform-details.single-game.suggestions .wrap01, .platform-details.single-game.suggestions .wrap02, .platform-details.single-game.suggestions .sub-title').css('display', 'none');
			$('.platform-details.single-game.suggestions .wrap03').show();
		});
		$('.platform-details.single-game.suggestions .wrap03 .cancel').on('click', function(){
			$('.platform-details.single-game.suggestions .wrap03').css('display', 'none');
			$('.wrap01').css('display', 'flex');
			$('.wrap01 .cancel').css('display', 'none');
			$('.wrap01 .submit').css('display', 'flex');
			$('.platform-details.single-game.suggestions .sub-title').show();
		});
	});
});

$(document).ready(function() {
	$('.about-wrap .right .he').on('click', function(){
		$(this).toggleClass('active');
		$(this).next().slideToggle();
	});
});


